create database FromHome

use FromHome

Create table Regist_FH(
ID_USER varchar (5) PRIMARY KEY NOT NULL,
FULL_NAME varchar (50) NOT NULL,
BIRTH_DATE varchar(13)NOT null,
GENDER varchar (6)NOT NULL ,
ADDRESS varchar (50) NOT NULL,
PROVINCE varchar (50) NOT NULL,
CITY varchar (50) NOT NULL,
PHONES int NOT NULL,
EMAIL varchar (30) NOT NULL,
USER_NAME varchar (15) NOT NULL,
PASSWORD varchar (15) NOT NULL,
);


create table Mens(
Merk varchar (80) PRIMARY KEY NOT NULL,
Size varchar (5) NOT NULL,
Price int NOT NULL,
Stock int NOT NULL
);

create table ComingSoon(
Release Date NOT NULL,
Merk varchar (80) PRIMARY KEY NOT NULL,
Size int NOT NULL,
);


create table Womens(
Merk varchar (80) PRIMARY KEY NOT NULL,
Size varchar (5) NOT NULL,
Price int NOT NULL,
Stock int NOT NULL
);

create table MensTlog(
Merk varchar (80)PRIMARY KEY NOT NULL,
Size varchar (5) NULL,
Price int NULL,
Quantity int NULL,
Total int NULL,
);

select * from Mens

insert into Mens (Merk, Size, Price, Stock)
 values ('Sweater RAVE HABBIT GANENDRA', 'M', 199500, 12),
('Sweater RAVE HABBIT KINNARD', 'M', 165000, 14),
('Outer Tahalim Dawn Drill Vest' , 'M', 230000, 10),
('Bomber Jacket 17seven Original ', 'L', 200000, 9),
('T Shirt Ocean Pacific ', 'L', 150000, 20),
('Short Sleeve Tee Tolliver V Neck Basic', 'XL', 100000, 14),
('Shirt Arnett', 'S', 200000, 15),
('Shirt WALRUS Studio', 'S', 150000, 13),
('Polo Shirt Country Fiesta', 'L', 150000, 13),
('Hoodies Tolliver Pull Over', 'L', 170000, 8),
('Training Pants Malibu', 'S', 150000, 19),
('Chinos Pants Denim Addict', 'L', 200000, 20),
('Short Cargo Pants Denim Addict', 'M', 170000, 11),
('Jogger Converse', 'L', 250000, 13),
('Short Pants Malibu Bermuda Rib', 'XL', 190000, 14),
('Pants Jimmy Martin Basic Regular Denim', 'M', 300000, 16),
('Pants Cotton On Hoff Shorts', 'L', 200000, 17),
('Jogger Shorts UniqTee', 'S', 1500000, 19),
('Pants CARVIL Bosero-Lb', 'L', 240000, 20),
('Pants Edwin Jeans', 'S', 250000, 21);

select * from ComingSoon
insert into ComingSoon (ReleaseDate,Merk, Size) 
values ('06/24/2020', 'Boots Aldo', 39),
('06/24/2020','Flatshoes Clarks', 41),
('06/24/2020','Heels Claymore', 36),
('06/24/2020','Flatshoes Fransisca Renaldy', 40),
('06/24/2020','Boots Java Seven', 40),
('06/24/2020','FlatshoesHead Over', 41),
('06/24/2020','Sneakers Kaninna Serena', 41),
('06/24/2020','Sneakers Keds', 40),
('06/24/2020','Boots HnM', 42),
('06/24/2020','Sneakers Hush Puppies' ,40),
('06/24/2020','Slip On Cotton On', 41),
('06/24/2020','Heels Zara', 41),
('06/24/2020','Flatshoes Mango', 42),
('06/24/2020','Boots Zara', 39),
('06/24/2020','Flatshoes HnM', 41);

select * from Womens
insert into Womens(Merk, Size, Price, Stock) 
values ('Dress GRAPHIS Knit Maxi', 'S',300000, 15),
('Blouse Seven Level Veronica', 'L', 250000, 11),
('Short Sleeves MISSGUIDED', 'M', 350000, 10),
('Sweater Rave Habbit ARELIA', 'L', 170000, 12),
('Sweater Tolliver Basic', 'S', 150000, 14),
('Cardigan WHITEMODE Cheyenne', 'L', 250000, 13),
('Cardigan Peponi Shacker Crop', 'M', 150000, 10),
('Long Sleeve Kimono Brave Soul Tess', 'S', 400000, 12),
('Blouse Icons Asym Pleated', 'M', 150000, 14),
(,'Tee Nike Sportswear', 'L', 220000, 15),
('Denim Shorts Something Borrowed', 'M', 200000, 15),
('Pants Hollister Stretch Oxford', 'L', 300000, 18),
('Pants Seven Level Jasmine', 'M', 150000, 15),
('Pants Something Borrowed Flare', 'L',  150000, 8),
('Pants Hollister Jogger', 'XL', 300000, 9),
('Trousers ZALIA BASICS', 'S', 200000, 17),
('Ripped Jeans River Island', 'L', 250000, 19),
('Skirt Hyde', 'XL', 230000, 10),
('Long Skirt ZAHRA SIGNATURE', 'XL', 180000, 13),
('Satin Skirt MISSGUIDED Black', 'M', 200000, 14);